import axios from "axios";

const key = '93dbec55a094cbfd2af50527e6731759';
const token = 'c28b8c32e7b021b04ef8939c8c5b5d7abe7191bc31a44e6c5c0746f6c11b195c';




// const key = process.env.key
// const token = process.env.token

export function getBoardData(){
    return axios.get(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`)
    .then(res=> res.data)
    .catch(err=> err)
    
}

export function addBoard(title){
    return axios.post(`https://api.trello.com/1/boards/?name=${title}&key=${key}&token=${token}`).then(res => res.data)
}

export function deleteBoard(id){
    axios.delete(`https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`)
}

export async function getLists(id){
    return await axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`).then(res=> res.data)
}

export async function addList(id, name){
    return await axios.post(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${key}&token=${token}`).then(res=> res.data)
}

export async function deleteList(id){
    return await axios.put(`https://api.trello.com/1/lists/${id}/closed?key=${key}&token=${token}&value=true`)
}


export async function getCards(id){
    return await axios.get(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`).then(res=> res.data)
}

export async function addCards(id, name){
    return await axios.post(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}&name=${name}`).then(res=> res.data)
}

export async function removeCard(id){
    return await axios.delete(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`).then(res=> 
    {
        return res.data;
    }
    )
}

export async function getCheckLists(id){
    return await axios.get(`https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`).then(res=> res.data)
}

export async function addCheckList(id, name){
    return await axios.post(`https://api.trello.com/1/checklists?idCard=${id}&key=${key}&token=${token}&name=${name}`).then(res=>{
        return res.data
    } )
}

export async function deleteCheckList(id){
    return await axios.delete(`https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`).then(res=> res.data)
}

export async function getCheckListItems(id){
    return await axios.get(`https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}`).then(res=> res.data)
}

export async function addCheckListItem(id, name){
    return await axios.post(`https://api.trello.com/1/checklists/${id}/checkItems?name=${name}&key=${key}&token=${token}`).then(res=>{
        return res.data
    } )
}


export async function deleteCheckItem(listId, itemId){
    return await axios.delete(`https://api.trello.com/1/checklists/${listId}/checkItems/${itemId}?key=${key}&token=${token}`).then(res=> res.data)
}

export async function updateCheckItem(itemId, newState, cardId, listId){
    return await axios.put(`https://api.trello.com/1/cards/${cardId}/checklist/${listId}/checkItem/${itemId}?key=${key}&token=${token}&state=${newState}`).then(res=> res.data)
}

// (itemId, newState, cardId, listId)

// https://api.trello.com/1/cards/$%7BcardId%7D/checklist/$%7BchecklistId%7D/checkItem/$%7BcheckItemId%7D?key=$%7Bkey%7D&token=$%7Btoken%7D&state=$%7Bstate%7D