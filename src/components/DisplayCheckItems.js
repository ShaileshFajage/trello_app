import React, { Component } from "react";



export class DisplayCheckItems extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         count : 0,
         isTrue : false
      }

    }


    // componentDidUpdate(){
    //   this.setState({
    //     count:this.state.count+1
    //   })
    //   console.log(this.state.count);
    // }


  render() {

    return (
      <div>
        <div style={{display:'flex', margin:'2rem'}}>
          <div>
          <input
            type="checkbox"
            id="subscribeNews"
            name="subscribe"
            value="newsletter"
            defaultChecked = {this.props.checkItem.state=='complete' ? true : false}
            onChange={(e)=>this.props.handleCheckItem(e, this.props.checkItem.id)}
          />
          <label style={{marginLeft:'1rem'}} htmlFor="subscribeNews"><h5>{this.props.checkItem.name}</h5></label>
        </div>
          <button style={{marginLeft:'1rem'}}
            onClick={() => this.props.deleteCheckItem(this.props.checkItem.id)}
          >
            Delete Item
          </button>
        </div>
      </div>
    );
  }
}
// onClick={() => this.props.deleteList(this.props.list.id)}
export default DisplayCheckItems;
