import React, { Component } from 'react'

import * as API from "./api";

import DisplayCheckList from "./DisplayCheckList"
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

export class CheckList extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         
        checkLists : []
      }
    }

    componentDidMount(){

      const id = this.props.match.params.cardId

        API.getCheckLists(id).then(res =>{
            this.setState({
                checkLists : res
            })

        })
    }

    addCheckList = async(e) =>{

        e.preventDefault()

        const name = e.target[0].value
        console.log(name);
        const cardId = this.props.match.params.cardId
        console.log(cardId);
        const newCheckItem = await API.addCheckList(cardId,name)

        this.setState({
          checkLists : [...this.state.checkLists, newCheckItem]
        })

    }

    deleteCheckList =  (id) =>{

      API.deleteCheckList(id)

      const lists = this.state.checkLists.filter(each =>{
        if(each.id!==id)
        {
          return each
        }
      })

      this.setState({
        checkLists : lists
      })

    }

  render() {

    // console.log(this.state.checkLists);

    // console.log(this.props.match.params.cardId);
    return (
      <>
      <Link to={`/${this.props.match.params.id}`} className="link">
          <Button variant="primary" size="sm">
            Back
          </Button>
        </Link>

      <div className='all-checklists'>
        <div className='checklists'>
            {this.state.checkLists.length==0 ? "Loading" :
            this.state.checkLists.map(list =>{
               return  <DisplayCheckList key={list.id} checkList={list} deleteCheckList={this.deleteCheckList} cardId={this.props.match.params.cardId}></DisplayCheckList>
            })
            }
        </div>

        
        <div>
        <form onSubmit={(e) => this.addCheckList(e)}>
          <input type="text" placeholder="Enter the checklist name"></input>
          <button type="submit">Add Checklist Item</button>
        </form>
        </div>
      </div>
      </>
    )
  }
}

export default CheckList