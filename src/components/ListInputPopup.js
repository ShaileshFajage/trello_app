import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from 'react-bootstrap/Form';

function InputPopup(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleBoardValue =  (e) => {
    e.preventDefault()

    handleClose()
    
    props.addListItem(e.target[0].value)
    
  };
  

  return (
    <>
      <Button className="list-item" onClick={handleShow} style={{ backgroundColor: "rgb(187, 127, 127)" }}>
        + Add a List
      </Button>

      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Body>
          
      <Form onSubmit={(e)=>handleBoardValue(e)}>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Enter the list name</Form.Label>
        <Form.Control type="text" placeholder="" />
      </Form.Group>
      
      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
          
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default InputPopup;
