import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import * as API from "./api";

import {
  Link
} from "react-router-dom";

function BoardDisplay(props) {

  // function deleteBoard(id){
  //   API.deleteBoard(id)
  // }
  return (
    <>
      <Link to={`/${props.board.id}`} className='card1' style={{textDecoration:'none'}}>
        <div>{props.board.name}</div>
        {/* <button onClick={deleteBoard(props.board.id)}>delete</button> */}
      </Link>
    </>
  );
}



export default BoardDisplay;