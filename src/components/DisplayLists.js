import React, { Component } from "react";

import * as API from "./api";

import DisplayCards from "./DisplayCards";

import {connect} from 'react-redux'

class DisplayLists extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cards: [],
      
    };
  }

  componentDidMount() {
    const listId = this.props.list.id;
    API.getCards(listId).then((res) => {
      this.setState({
        cards: res,
      });


    });
  }

  listHandler = async (e) => {
    e.preventDefault()
    const listId = this.props.list.id;

    const cardName = e.target[0].value;

    console.log(cardName);

    const newCard = await API.addCards(listId, cardName);

    this.setState({
      cards: [...this.state.cards, newCard],
    });
  };


  removeCard = async (cardId) => {
   
    await API.removeCard(cardId);
    // window.location.reload();
    const finalCards = this.state.cards.filter(each=>{
        if(each.id!==cardId)
        {
            return each;
        }
    })

    this.setState({
        cards : finalCards
    })
  };

  render() {

    return (
      <div className="list-item">
        <div className="list-name">{this.props.list.name}</div>
        {this.state.cards.map((card) => {
          return (
            <DisplayCards
              removeCard = {this.removeCard}
              key={card.id}
              card={card}
              cards={this.props.cards}
              listId={this.props.list.id}
              boardId={this.props.boardId}
            ></DisplayCards>
          );
        })}

        <form onSubmit={(e) => this.listHandler(e)}>
          <input type="text" placeholder="Enter the name of  card"></input>
          <button type="submit">Add Card</button>
        </form>

        {/* <button onClick={() => this.listHandler()}>+ Add card</button> */}
        <button onClick={() => this.props.deleteList(this.props.list.id)}>
          Delete list
        </button>
      </div>

    );
  }
}

export default DisplayLists;
