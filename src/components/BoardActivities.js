import React, { Component } from "react";

import * as API from "./api";

import DisplayLists from "./DisplayLists";
import Button from "react-bootstrap/Button";
import ListInputPopup from "./ListInputPopup"

import { Link } from "react-router-dom";

class BoardActivities extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lists: [],
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    API.getLists(id).then((res) => {
      this.setState({
        lists: res,
      });
    });
  }

  addListItem = async (name) => {
    const id = this.props.match.params.id;
    // const name = prompt("Enter list name");

    // if (name == null) {
    //   return;
    // }

    const newList = await API.addList(id, name);

    this.setState({
      lists: [newList, ...this.state.lists],
    });

  };

  deleteList = async (listId) => {

    await API.deleteList(listId);

    const finalLists = this.state.lists.filter((each) => {
      if (each.id !== listId) {
        return each;
      }
    });

    this.setState({
      lists: finalLists
    });

  };

  render() {
    return (
      <>
        <Link to="/" className="link">
          <Button variant="primary" size="sm">
            Back
          </Button>
        </Link>
        <div className="outer-board">
          <div className="board-container">
            {/* {this.props.match.params.id} */}
            {this.state.lists.map((list) => {
              return (
                <DisplayLists
                  deleteList={this.deleteList}
                  key={list.id}
                  list={list}
                  boardId={this.props.match.params.id}
                />
              );
            })}
          </div>

          <div>
            {/* <button
              onClick={() => this.addListItem()}
              className="list-item"
              style={{ backgroundColor: "rgb(187, 127, 127)" }}
            >
              + Add a List
            </button> */}
            <ListInputPopup addListItem={this.addListItem}></ListInputPopup>
          </div>
        </div>
      </>
    );
  }
}

export default BoardActivities;
