import { faWindowRestore } from '@fortawesome/free-regular-svg-icons';
import React, { Component } from 'react'
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

import {
  Link
} from "react-router-dom";


import * as API  from "./api";

class DisplayCards extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         cards : []
      }
    }

    componentDidMount(){
      API.getCards(this.props.listId).then(res=> {
        this.setState({
          cards : res.data
      })
      })
       
    }

    
  render() {
    return (
      <div className='oneCard' >
        <Link to={`/${this.props.boardId}/${this.props.card.id}`} style={{textDecoration:'none'}}>
        <p style={{backgroundColor:'green', color:'white', border:'1px solid black'}}>{this.props.card.name}</p>
        </Link>
        <button onClick={()=>this.props.removeCard(this.props.card.id)}>Remove card</button>
      </div>


    )
  }
}

export default DisplayCards