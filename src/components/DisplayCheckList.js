import React, { Component } from "react";
import { ThemeProvider } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import * as API from "./api";
import DisplayCheckItems from "./DisplayCheckItems";

import ProgressBar from 'react-bootstrap/ProgressBar';


export class DisplayCheckList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkItems: [],
    
    };
  }

  componentDidMount() {
    const id = this.props.checkList.id;
    API.getCheckListItems(id).then((res) => {
      this.setState({
        checkItems: res
      });
    });
  }

  addCheckListItem = async (e) => {
    e.preventDefault();

    const name = e.target[0].value;
    const id = this.props.checkList.id;
    const newItem = await API.addCheckListItem(id, name);

    this.setState({
      checkItems: [...this.state.checkItems, newItem],
    });
  };

  deleteCheckItem = async (itemId) => {

    const listId = this.props.checkList.id;

    await API.deleteCheckItem(listId, itemId)

    const filteredItems = this.state.checkItems.filter((each) => {
      if (each.id !== itemId) {
        return each;
      }
    });

    this.setState({
      checkItems: filteredItems
    });
  };

  handleCheckItem = async (e, itemId) =>{
    const listId = this.props.checkList.id;
    const cardId = this.props.cardId;

    const newState = e.target.checked===true ? 'complete' : 'incomplete'

    // this.setState({
    //   state : newState
    // })

    API.updateCheckItem(itemId, newState, cardId, listId).then()

    // handleUpdate(itemId, newState, listId)
    
  }

  render() {


    return (
      <div className="outer-checklist-div">
        <div className="each-checklist">
          <h3>{this.props.checkList.name}</h3>

          <div style={{}}>
            <button
              style={{ border: "1px solid black" }}
              onClick={() =>
                this.props.deleteCheckList(this.props.checkList.id)
              }
            >
              Delete Checklist
            </button>
          </div>
        </div>

        <div>
        {/* <ProgressBar now={now} label={`${now}%`} visuallyHidden /> */}

          {this.state.checkItems.length == 0 ? (
            <p></p>
          ) : (
            this.state.checkItems.map((item) => {
              return (
                <DisplayCheckItems
                  key={item.id}
                  checkItem={item}
                  deleteCheckItem={this.deleteCheckItem}
                  handleCheckItem = {this.handleCheckItem}
                ></DisplayCheckItems>
              );
            })
          )}
        </div>

        <div>
          <form onSubmit={(e) => this.addCheckListItem(e)}>
            <input
              type="text"
              placeholder="Enter the check item"
            ></input>
            <button type="submit">Add Item</button>
          </form>
        </div>
      </div>
    );
  }
}

export default DisplayCheckList;
