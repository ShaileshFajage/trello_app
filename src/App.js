import "react-bootstrap";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import React, { Component } from "react";

import * as API from "./components/api";

import BoardDisplay from "./components/BoardDisplay";
import Navbar from "./components/Navbar";
import BoardActivities from "./components/BoardActivities";
import CheckList from "./components/CheckList";

import InputPopup from "./components/InputPopup";

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      boards: [],
      
    };
  }

  componentDidMount() {
    API.getBoardData().then((res) => {
      this.setState({
        boards: res,
      });
    })
    
  }

  createNewBoard = async (title) => {
    

    console.log(title);

    const newBoard = await API.addBoard(title);

    this.setState({
      boards: [...this.state.boards, newBoard],
    });
  };

  render() {

    return (
      <>
        <Navbar></Navbar>

        <Router>
          <Switch>
            <Route exact path="/">
              <div className="home">
                {this.state.boards.length === 0 ? (
                  <h1>Loading...</h1>
                ) : (
                  this.state.boards.map((board) => {
                    return (
                      <BoardDisplay key={board.id} board={board}></BoardDisplay>
                    );
                  })
                )}
                

             <InputPopup createNewBoard={this.createNewBoard}></InputPopup>


              </div>
             
            </Route>

            <Route exact path="/:id" component={BoardActivities}></Route>

            <Route path="/:id/:cardId" component={CheckList}></Route>
          </Switch>
        </Router>
      </>
    );
  }
}

export default App;
